import * as React from "react"
import { Link, graphql } from "gatsby"
import Layout from "../components/layout"

const IndexPage = props => {
  console.log(props)

  return (
    <Layout>
      {props.data.allBook.edges.map(({ node }) => (
        <div key={node.id}>
          <h2>
            {node.title} - <small>{node.author.name}</small>
          </h2>
          <div>{node.summary}</div>
          <Link to={`/book/${node.id}`}>Join conversation</Link>
        </div>
      ))}
    </Layout>
  )
}
export const query = graphql`
  {
    allBook {
      edges {
        node {
          summary
          title
          id
          author {
            name
          }
        }
      }
    }
  }
`

export default IndexPage
